﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{
    void Update()
    {
        float x = Mathf.Cos(Time.deltaTime) * 2;
        transform.position = new Vector3(x, transform.position.y, transform.position.z);
    }
}
