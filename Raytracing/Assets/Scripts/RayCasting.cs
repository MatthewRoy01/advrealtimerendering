﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayCasting : MonoBehaviour
{
    private Texture2D tex;
    public int width;
    public int height;
    public float updateSpeed;

    private void Awake()
    {
        tex = new Texture2D(width, height);
    }

    private void Start()
    {
        StartCoroutine("Cast");
    }

    private IEnumerator Cast()
    {
        while(true)
        {
            for (int w = 0; w < width; ++w)
            {
                for (int h = 0; h < height; ++h)
                {
                    Ray ray = Camera.main.ViewportPointToRay(new Vector3((float)w / width, (float)h / height, Camera.main.nearClipPlane));

                    RaycastHit hit;

                    bool didHit = Physics.Raycast(ray, out hit);

                    if (didHit)
                    {
                        ShaderScript currentShader = hit.transform.GetComponent<ShaderScript>();
                        if (currentShader != null)
                        {
                            tex.SetPixel(w, h, currentShader.calcColor(hit));
                        }
                        else
                        {
                            // if no shader, just color it black (no light reflected)
                            tex.SetPixel(w, h, Color.black);
                        }
                    }
                    else
                    {
                        tex.SetPixel(w, h, Color.black);
                    }
                }
            }

            tex.Apply();

            yield return new WaitForSeconds(updateSpeed);
        }
    }

    private void OnGUI()
    {
        GUI.DrawTexture(new Rect(0.0f, 0.0f, Screen.width, Screen.height), tex);
    }

    // leftover draw texture function that probably overthought how UVs work
    /*
    private Vector4 DrawTexture(RaycastHit hit, int w, int h)
    {
        Vector4 texCoord = hit.textureCoord;
        Vector4 UVcolor = beetleTex.GetPixel((int)(texCoord.x * beetleTex.width), (int)(texCoord.y * beetleTex.height));
        UVcolor = UVcolor.normalized;
        UVcolor.w = 1.0f;
        return UVcolor;
    }
    */
}
