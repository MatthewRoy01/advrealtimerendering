﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShaderLambertDiffuse : ShaderScript
{
    public Texture2D mainTex;
    public Light mainLight;

    private float shadowNorm = 0.02f;

    override public Color calcColor(RaycastHit hit)
    {
        if (mainLight == null)
        {
            Debug.LogWarning("Light not connected to shader");
            return Color.black;
        }

        // get normals from the mesh
        Mesh mesh = (hit.collider as MeshCollider).sharedMesh;
        Vector3[] normals = mesh.normals;
        int[] triangles = mesh.triangles;
        Vector3 n0 = normals[triangles[hit.triangleIndex * 3 + 0]];
        Vector3 n1 = normals[triangles[hit.triangleIndex * 3 + 1]];
        Vector3 n2 = normals[triangles[hit.triangleIndex * 3 + 2]];

        // find average normal
        Vector3 baryCenter = hit.barycentricCoordinate;
        Vector3 interpolatedNormal = n0 * baryCenter.x + n1 * baryCenter.y + n2 * baryCenter.z;

        // vector to light
        Vector3 L = (mainLight.transform.position - hit.point).normalized;
        // normal vector
        Vector3 N = interpolatedNormal;

        // check if something is in shadow
        int isShadow = Physics.Raycast(hit.point + (hit.normal * shadowNorm), mainLight.transform.position - hit.point, Vector3.Distance(hit.point + (hit.normal * shadowNorm), mainLight.transform.position)) ? 0 : 1;

        // lambert and diffuse
        Color lambert = mainTex.GetPixelBilinear(hit.textureCoord.x, hit.textureCoord.y);
        float diffuse = Vector3.Dot(N, L);
        diffuse = Mathf.Max(diffuse, 0.0f) * (mainLight.intensity * isShadow);

        // ambient
        Color ambient = mainLight.color;

        // get color from texture using texcoords
        Vector4 fragColor = lambert * diffuse * ambient;

        // set alpha to 1.0f
        fragColor.w = 1.0f;

        return fragColor;
    }
}
