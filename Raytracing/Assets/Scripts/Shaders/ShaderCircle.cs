﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShaderCircle : ShaderScript
{
    public Vector2 uvPos;
    public float radius;
    public Color color;

    private void Start()
    {
        uvPos = transform.position;
    }
    override public Color calcColor(RaycastHit hit)
    {
        Color fragColor;

        Vector2 circlePosition = new Vector2(0.5f, 0.5f);

        if ((hit.textureCoord - circlePosition).magnitude < radius)
        {
            fragColor = color;
        }
        else
        {
            fragColor = hit.transform.GetComponent<Renderer>().sharedMaterial.color;
        }

        return fragColor;
    }
}
