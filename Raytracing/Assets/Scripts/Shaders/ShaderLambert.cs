﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShaderLambert : ShaderScript
{
    public Texture2D mainTex;

    override public Color calcColor(RaycastHit hit)
    {
        // get color from texture using texcoords
        Vector4 fragColor = mainTex.GetPixelBilinear(hit.textureCoord.x, hit.textureCoord.y);

        // set alpha to 1.0f
        fragColor.w = 1.0f;

        return fragColor;
    }
}
