﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShaderPhong : ShaderScript
{
    public Texture2D mainTex;
    public Light mainLight;
    public float shadowNorm = 0.02f;
    public Color ambient;

    private bool directional;
    RaycastHit hit;

    override public Color calcColor(RaycastHit newHit)
    {
        // save the raycast hit
        hit = newHit;

        // check for the light
        if (mainLight == null)
        {
            Debug.LogWarning("Light not connected to shader");
            return Color.black;
        }
        else
        {
            directional = mainLight.type == LightType.Directional;
        }

        // vector to light
        Vector3 L = GetL();

        // normal vector
        Vector3 N = GetN();

        // reflection vector
        Vector3 R = GetR(N, L);

        // vector to viewer
        Vector3 V = GetV();

        // check if something is in shadow
        int isShadow = GetShadow();

        // lambert
        Color lightTint = mainLight.color;
        Color lambert = mainTex.GetPixelBilinear(hit.textureCoord.x, hit.textureCoord.y) * lightTint;

        // diffuse
        float diffuseF = Vector3.Dot(N, L);
        diffuseF = Mathf.Max(diffuseF, 0.0f) * (GetLightIntensity() * isShadow);
        Vector4 diffuse = lambert * diffuseF;
        diffuse.w = 1.0f;

        // specular
        float specularF = Mathf.Pow(Vector3.Dot(V, R), 6);
        if (Vector3.Dot(V, R) < 0.5f)
        {
            specularF = 0.0f;
        }
        Vector4 specular = Color.white * specularF;
        specular.w = 1.0f;

        // do phong shading
        Vector4 fragColor = diffuse + specular + (Vector4)ambient;

        // set alpha to 1.0f
        fragColor.w = 1.0f;

        return fragColor;
    }

    // get the light vector
    private Vector3 GetL()
    {
        // return a direction to the light
        return directional ? mainLight.transform.forward * -1 : (mainLight.transform.position - hit.point).normalized;
    }

    // get the normal vector
    private Vector3 GetN()
    {
        // get normals from the mesh
        Mesh mesh = (hit.collider as MeshCollider).sharedMesh;
        Vector3[] normals = mesh.normals;
        int[] triangles = mesh.triangles;
        Vector3 n0 = normals[triangles[hit.triangleIndex * 3 + 0]];
        Vector3 n1 = normals[triangles[hit.triangleIndex * 3 + 1]];
        Vector3 n2 = normals[triangles[hit.triangleIndex * 3 + 2]];

        // find average normal
        Vector3 baryCenter = hit.barycentricCoordinate;
        Vector3 interpolatedNormal = n0 * baryCenter.x + n1 * baryCenter.y + n2 * baryCenter.z;
        return interpolatedNormal;
    }

    // get the reflection vector
    private Vector3 GetR(Vector3 N, Vector3 L)
    {
        return 2 * Vector3.Dot(N, L) * (N - L);
    }

    // get the vector to the viewer
    private Vector3 GetV()
    {
        return (Camera.main.gameObject.transform.position - hit.point).normalized;
    }

    private int GetShadow()
    {
        if (directional)
        {
            // fire a raycast according the direction of the light
            // fire it with infinite range
            return Physics.Raycast(
                hit.point + (hit.normal * shadowNorm),
                mainLight.transform.forward * -1)
                ? 0 : 1;
        }
        else
        {
            // fire a raycast from the point light to the raycast hit point
            // fire it with the appropriate range back to the light
            return Physics.Raycast(
                hit.point + (hit.normal * shadowNorm),
                mainLight.transform.position - hit.point,
                Vector3.Distance(hit.point + (hit.normal * shadowNorm), mainLight.transform.position))
                ? 0 : 1;
        }
    }

    private float GetLightIntensity()
    {
        if (directional)
        {
            return mainLight.intensity;
        }
        else
        {
            // get the distance
            float distance = Vector3.Distance(hit.point, mainLight.transform.position);

            // if the raycast hit is outside the light's range, decrease the intensity as we get farther away
            if (distance > mainLight.range)
            {
                // decrease the intensity linearly
                return mainLight.intensity - ((distance - mainLight.range) * 0.1f);
            }
            else
            {
                return mainLight.intensity;
            }
        }
    }
}
